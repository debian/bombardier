/* Bombardier */
/* The GNU Bombing utility */
/* Copyright (C) 2001, 2009 Gergely Risko */
/* Can be licensed under the terms of GPL v3 or above */

#include "incs.h"
#include "values.h"
#include "structs.h"
#include "funcs.h"
#include "extern.h"
