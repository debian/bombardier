/* Bombardier */
/* The GNU Bombing utility */
/* Copyright (C) 2001, 2009 Gergely Risko */
/* Can be licensed under the terms of GPL v3 or above */

#include "bombardier.h"

int gx(int x)
{
    return x+(maxx-WIDTH)/2;
}

int gy(int y)
{
    return y+(maxy-HEIGHT)/2;
}
