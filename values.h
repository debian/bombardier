/* Bombardier */
/* The GNU Bombing utility */
/* Copyright (C) 2001, 2009 Gergely Risko */
/* Can be licensed under the terms of GPL v3 or above */

#define WIDTH 80
#define HEIGHT 24
#define DESTROY 7
#define MINSIZE 0
#define MAXSIZE 20
#define USECSLEEP 120000
#define BOMBFAST 2
#define DEFBLOCK 500
#define ADDBLOCK 400
